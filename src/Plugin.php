<?php
/**
 *
 *
 * @author 耐小心<i@naixiaoxin.com>
 * @time      2019-02-12
 *
 * @copyright 2018-2019 耐小心
 */

namespace Kanong\Installer;


use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Kanong\Installer\Discuz\Plugin as DiscuzPlugin;

class Plugin implements PluginInterface
{
    public function activate(Composer $composer, IOInterface $io)
    {
        $manager = $composer->getInstallationManager();
        // DiscuzX插件
        $manager->addInstaller(new DiscuzPlugin($io, $composer));
    }

}
