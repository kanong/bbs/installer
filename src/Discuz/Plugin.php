<?php
/**
 *
 *
 * @author 耐小心<i@naixiaoxin.com>
 * @time      2019-02-12
 *
 * @copyright 2018-2019 耐小心
 */


namespace Kanong\Installer\Discuz;

use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;

class Plugin extends LibraryInstaller
{


    /**
     * 定义安装目录
     *
     * @param PackageInterface $package
     * @return string
     */
    public function getInstallPath(PackageInterface $package)
    {
        if ($this->composer->getPackage()->getType() !== 'project') {
            return parent::getInstallPath($package);
        }
        $config = $package->getExtra();
        if(isset($config["pluginid"])){
            $pluginID = $config['pluginid'];
        }else{
            $pluginTmp = explode("/",$package->getName());
            if (count($pluginTmp) == 1) {
                $pluginID = $package->getName();
            }else{
                $pluginID = $pluginTmp[1];
            }
        }

        return 'source/plugin/' .$pluginID;
    }

    /**
     * 判断是否是DiscuzX插件
     *
     * @param $packageType
     * @return bool
     */
    public function supports($packageType)
    {
        return $packageType == "discuz-plugin";
    }
}